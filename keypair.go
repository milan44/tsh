package tsh

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha512"
	"encoding/hex"
	"errors"
	mrand "math/rand"
)

type KeyPair struct {
	Private rsa.PrivateKey
	Public  rsa.PublicKey
}

func Encrypt(bytes []byte, public rsa.PublicKey) (string, error) {
	chunks := _explode(bytes)
	hash := sha512.New()
	result := ""

	for _, chunk := range chunks {
		ciphertext, err := rsa.EncryptOAEP(hash, rand.Reader, &public, []byte(chunk), nil)
		if err != nil {
			return "", err
		}

		result += _encode(ciphertext) // 1536 characters long (no matter how long the chunk is)
	}

	if len(result) == 0 {
		return "", nil
	}

	return result, nil
}
func Decrypt(ciphertext string, private rsa.PrivateKey) ([]byte, error) {
	chunks := _unjoin(ciphertext)
	result := make([]string, 0)

	for _, chunk := range chunks {
		hash := sha512.New()

		decoded, err := _decode(chunk)
		if err != nil {
			return nil, err
		}

		plaintext, err := rsa.DecryptOAEP(hash, rand.Reader, &private, decoded, nil)
		if err != nil {
			return nil, err
		}
		hexEncoded := string(plaintext)

		result = append(result, hexEncoded)
	}

	if len(result) == 0 {
		return nil, errors.New("empty result")
	}

	return _implode(result)
}

func NewKeyPair() (KeyPair, error) {
	privkey, err := rsa.GenerateKey(rand.Reader, 6144)
	if err != nil {
		return KeyPair{}, err
	}

	keypair := KeyPair{
		Private: *privkey,
		Public:  (*privkey).PublicKey,
	}

	return keypair, nil
}

func _unjoin(str string) []string {
	chunkLength := 1536
	var divided []string
	for i := 0; i < len(str); i += chunkLength {
		end := i + chunkLength

		chunk := str[i:end]

		divided = append(divided, chunk)
	}
	return divided
}
func _explode(bytes []byte) []string {
	chunkSize := 250
	var divided []string
	for i := 0; i < len(bytes); i += chunkSize {
		end := i + chunkSize

		if end > len(bytes) {
			end = len(bytes)
		}

		chunk := bytes[i:end]

		divided = append(divided, _encode(chunk)+_randStringBytes(20))
	}
	return divided
}
func _implode(chunks []string) ([]byte, error) {
	var result []byte
	for _, chunk := range chunks {

		decoded, err := _decode(chunk[:len(chunk)-20])
		if err != nil {
			return nil, err
		}

		result = append(result, decoded...)
	}
	return result, nil
}
func _encode(bytes []byte) string {
	return hex.EncodeToString(bytes)
}
func _decode(str string) ([]byte, error) {
	return hex.DecodeString(str)
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func _randStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[mrand.Intn(len(letterBytes))]
	}
	return string(b)
}
