# Tor Secure HTTP (TSH) ![pipeline](https://gitlab.com/milan44/tsh/badges/master/pipeline.svg)

![forthebadge](https://forthebadge.com/images/badges/designed-in-ms-paint.svg)
![forthebadge](https://forthebadge.com/images/badges/made-with-go.svg)
![forthebadge](https://forthebadge.com/images/badges/you-didnt-ask-for-this.svg)

Secure communication using private/public key encryption for unencrypted http communication via tor.
That extra bit security, that you don't actually need but it makes you feel save.