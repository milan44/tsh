package main

import "gitlab.com/milan44/tsh"

const ExampleText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."

func keyPairEnAndDecryptionTest() {
	pair, err := tsh.NewKeyPair()
	if err != nil {
		panic(err)
	}

	encrypted, err := tsh.Encrypt([]byte(ExampleText), pair.Public)
	if err != nil {
		panic(err)
	}

	decrypted, err := tsh.Decrypt(encrypted, pair.Private)
	if err != nil {
		panic(err)
	}

	if ExampleText != string(decrypted) {
		panic("en-/decryption failed")
	}
}
