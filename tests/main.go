package main

import (
	"fmt"
	"reflect"
	"runtime"
	"strings"
	"time"
)

func main() {
	fmt.Println("###################")
	fmt.Println("#    TSH Tests    #")
	fmt.Println("###################")
	fmt.Println("")

	runTest(keyPairEnAndDecryptionTest)
}

func runTest(test func()) {
	name := runtime.FuncForPC(reflect.ValueOf(test).Pointer()).Name()
	fmt.Print(strings.ReplaceAll(name, "main.", "") + "() ")
	start := time.Now()
	test()
	fmt.Println("success (" + fmt.Sprintf("%s", time.Since(start)) + ")")
}
